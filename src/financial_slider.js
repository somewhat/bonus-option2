import React, { Component } from 'react';

class FinancialSlider extends Component {
  render() {
	  
    return (
      <div className="financial-wrap clearfix" id="financial-slider-outer">
	  	<div className="inner-wrap">
        	<div className="f-right w-50 financial-slider-wrap" >
            	Financial concerns plague <br className="mobile" />workers at all levels of <br className="mobile" />your organization
				<div className="financial-slider-inner desktop" id="financial-slider">
					<ul className="financial-slider desktop"  id="financial-slider-count" style={{left:0,top:0}} >
						<li >
							<span className="count">85<sup>%</sup></span>
							<span className="count-desc">of American workers say they use <br />time during the workday to deal <br />with their personal financial <br />issues</span>
						</li>
						<li >
							<span className="count">75<sup>%</sup></span>
							<span className="count-desc">of workers at least sometimes live <br />paycheck-to-paycheck to make <br />ends meet, and 38% are regularly <br />stuck in this cycle</span>
						</li>
						<li >
							<span className="count">44<sup>%</sup></span>
							<span className="count-desc">of adults lack the funds to cover a <br />$400 emergency</span>
						</li>
					</ul>
				</div>
				<ul className="financial-slider mobile"  >
                	<li>
                    	<span className="count">85<sup>%</sup></span>
                        <span className="count-desc">of American workers say they use time during the workday to deal with their personal financial issues</span>
                    </li>
                    <li>
                    	<span className="count">75<sup>%</sup></span>
                        <span className="count-desc">of workers at least sometimes live paycheck-to-paycheck to make ends meet, and 38% are regularly stuck in this cycle</span>
                    </li>
                    <li>
                    	<span className="count">44<sup>%</sup></span>
                        <span className="count-desc">of adults lack the funds to cover a $400 emergency</span>
                    </li>
				</ul>                    
            </div>
			</div>
        </div>
    );
  }
}


export default FinancialSlider;
