import React, { Component } from 'react';
import banner_desktop from './images/top-image.png';
import banner_mobile from './images/top-image-mobile.png';

class TopBanner extends Component {
  render() {
    return (
      <div className="top-banner">
        	<img alt="" src={banner_desktop} className="desktop" />
            <img alt="" src={banner_mobile} className="mobile" />
			<div className="inner-wrap">
				<div className="v-align-middle">
					<div>
						<div className="sub-heading">for business</div>
						<div className="main-heading">MONEYLION<br />BONUS</div>
						<div className="desc">Premium financial <br className="desktop" />services <br className="mobile" />for America's <br className="desktop"/>hardworking <br className="mobile"/>employees.</div>
					</div>
				</div>
			</div>
        </div>
    );
  }
}


export default TopBanner;
