import React, { Component } from 'react';
import './App.css';

import Header from './header';
import Footer from './footer';
import TopBanner from './topbanner';
import BusinessSlider from './business_slider';
import FinancialSlider from './financial_slider';
import BonusWrap from './bonus_wrap';
import EmployeeSlider from './employee_slider';
import GettingWrap from './getting_wrap';
import FormsWrap from './forms_wrap';


class App extends Component {
  render() {
    return (
      <div>
        <Header />
		<TopBanner />
		<BusinessSlider />
		<FinancialSlider />
		<BonusWrap />
		<EmployeeSlider />
		<GettingWrap />
		<FormsWrap />
		<Footer />
	  </div>
    );
  }
  componentDidMount() {
    var isSafari = /^((?!chrome|android).)*safari/i.test(navigator.userAgent);
    if(navigator.userAgent.indexOf('Mac') > 0){
		document.body.classList.add('mac-os');
	}
	//var businessY = 0;
	var financialY = 0;
	var empY = 0;
	var empMax = 0;
	
	var isScrolling;
	var financialCount = 0;
	var empCount = 0;
	// GET OFFSET POSITION
	function getOffset( el ) {
		var _x = 0;
		var _y = 0;
		while( el && !isNaN( el.offsetLeft ) && !isNaN( el.offsetTop ) ) {
			_x += el.offsetLeft - el.scrollLeft;
			_y += el.offsetTop - el.scrollTop;
			el = el.offsetParent;
		}
		return { top: _y, left: _x };
	}
	//var wHeight = window.innerHeight;
	setTimeout(function() {
		//businessY = Math.round(getOffset( document.getElementById('business-slider') ).top-((window.innerHeight-600)/2));
		financialY = Math.round(getOffset( document.getElementById('financial-slider-outer') ).top-((window.innerHeight-600)/2)-47);
		empY = Math.round(getOffset( document.getElementById('emp-slider-outer') ).top-((window.innerHeight-450)/2)-47);
		
		empMax = (document.getElementById('emp-slider-count').offsetHeight/2)+25;
	}, 1000);
	window.addEventListener("orientationchange", function() {
    	//businessY = Math.round(getOffset( document.getElementById('business-slider') ).top-((window.innerHeight-600)/2));
		financialY = Math.round(getOffset( document.getElementById('financial-slider-outer') ).top-((window.innerHeight-600)/2)-47);
		empY = Math.round(getOffset( document.getElementById('emp-slider-outer') ).top-((window.innerHeight-450)/2)-47);
		
		empMax = (document.getElementById('emp-slider-count').offsetHeight/2)+25;
	});
	document.addEventListener('keydown', (event) => {
	  var keyName = event.keyCode;
	  //console.log(keyName);
	  if(keyName === 40){
		 windowScroll = true;
	  }
	  if(keyName === 38){
		 windowScroll = false;
	  }
	});
	
	window.addEventListener('scroll', function ( event ) {
		
		if(!isSafari){
			financialY = Math.round(getOffset( document.getElementById('financial-slider-outer') ).top-((window.innerHeight-600)/2)-47);
			empY = Math.round(getOffset( document.getElementById('emp-slider-outer') ).top-((window.innerHeight-450)/2)-47);
			
			empMax = (document.getElementById('emp-slider-count').offsetHeight/2)+25;
		}
		
		if(window.scrollY <= financialY){
			/*document.getElementById('financial-slider-count').getElementsByTagName('li')[0].style.opacity = '1';
			document.getElementById('financial-slider-count').getElementsByTagName('li')[1].style.opacity = '0';
			document.getElementById('financial-slider-count').getElementsByTagName('li')[2].style.opacity = '0';*/
			document.getElementById('financial-slider-count').getElementsByTagName('li')[0].classList.add('active');
			document.getElementById('financial-slider-count').getElementsByTagName('li')[1].classList.remove('remove');
			document.getElementById('financial-slider-count').getElementsByTagName('li')[2].classList.remove('remove');
		}
		if(window.scrollY-financialY >= 1 && window.scrollY-financialY <= 30){
			/*document.getElementById('financial-slider-count').getElementsByTagName('li')[0].style.opacity = '1';
			document.getElementById('financial-slider-count').getElementsByTagName('li')[1].style.opacity = '0';
			document.getElementById('financial-slider-count').getElementsByTagName('li')[2].style.opacity = '0';*/
			document.getElementById('financial-slider-count').getElementsByTagName('li')[0].classList.add('active');
			document.getElementById('financial-slider-count').getElementsByTagName('li')[1].classList.remove('active');
			document.getElementById('financial-slider-count').getElementsByTagName('li')[2].classList.remove('active');
		}
		if(window.scrollY-financialY >= 31 && window.scrollY-financialY <= 210){
			/*document.getElementById('financial-slider-count').getElementsByTagName('li')[0].style.opacity = '0';
			document.getElementById('financial-slider-count').getElementsByTagName('li')[1].style.opacity = '1';
			document.getElementById('financial-slider-count').getElementsByTagName('li')[2].style.opacity = '0';*/
			document.getElementById('financial-slider-count').getElementsByTagName('li')[0].classList.remove('active');
			document.getElementById('financial-slider-count').getElementsByTagName('li')[1].classList.add('active');
			document.getElementById('financial-slider-count').getElementsByTagName('li')[2].classList.remove('active');	
		}
		if(window.scrollY-financialY >= 211 && window.scrollY-financialY <= 300){
			/*document.getElementById('financial-slider-count').getElementsByTagName('li')[0].style.opacity = '0';
			document.getElementById('financial-slider-count').getElementsByTagName('li')[1].style.opacity = '0';
			document.getElementById('financial-slider-count').getElementsByTagName('li')[2].style.opacity = '1';*/
			document.getElementById('financial-slider-count').getElementsByTagName('li')[0].classList.remove('active');
			document.getElementById('financial-slider-count').getElementsByTagName('li')[1].classList.remove('active');
			document.getElementById('financial-slider-count').getElementsByTagName('li')[2].classList.add('active');	
		}
		if(window.scrollY-financialY >= 301){
			/*document.getElementById('financial-slider-count').getElementsByTagName('li')[0].style.opacity = '0';
			document.getElementById('financial-slider-count').getElementsByTagName('li')[1].style.opacity = '0';
			document.getElementById('financial-slider-count').getElementsByTagName('li')[2].style.opacity = '1';	*/
			document.getElementById('financial-slider-count').getElementsByTagName('li')[0].classList.remove('active');
			document.getElementById('financial-slider-count').getElementsByTagName('li')[1].classList.remove('active');
			document.getElementById('financial-slider-count').getElementsByTagName('li')[2].classList.add('active');	
		}
		
		if(window.innerWidth >= 1024){
			if(windowScroll){
				
				/*if(window.scrollY >= financialY){
					if(financialCount < 3){
					//if(parseInt(document.getElementById('financial-slider-count').style.top) > -500){
						window.scrollTo(0,financialY);
						if(financialCount === 1){
							if(parseInt(document.getElementById('financial-slider-count').style.top) >= -250){
								document.getElementById('financial-slider-count').style.top = (parseInt(document.getElementById('financial-slider-count').style.top)-10)+"px";
							}
							if(parseInt(document.getElementById('financial-slider-count').style.top) <= -250){
								document.getElementById('financial-slider-count').style.top = -250+"px";
							}
						}
						if(financialCount === 2){
							if(parseInt(document.getElementById('financial-slider-count').style.top) >= -500){
								document.getElementById('financial-slider-count').style.top = (parseInt(document.getElementById('financial-slider-count').style.top)-10)+"px";
							}
							if(parseInt(document.getElementById('financial-slider-count').style.top) <= -500){
								document.getElementById('financial-slider-count').style.top = -500+"px";
							}
						}
						if(parseInt(document.getElementById('financial-slider-count').style.top) < -500){
							
							document.getElementById('financial-slider-count').style.top = -500+"px";
						}
						document.getElementById('financial-slider-count').classList.remove('set-animate');
						window.clearTimeout( isScrolling );
						isScrolling = setTimeout(function() {
							document.getElementById('financial-slider-count').classList.add('set-animate');
							financialCount++;
							//console.log(financialCount+'      '+document.getElementById('financial-slider-count').style.top);
							if(financialCount === 2){
								document.getElementById('financial-slider-count').style.top = -250+"px";
							}
							if(financialCount === 3){
								document.getElementById('financial-slider-count').style.top = -500+"px";
								
							}
						}, 500);
						
					}
				}*/
				if(window.scrollY >= empY){
					if(parseInt(document.getElementById('emp-slider-count').style.top) > -(empMax)){
						window.scrollTo(0,empY);
						document.getElementById('emp-slider-count').style.top = (parseInt(document.getElementById('emp-slider-count').style.top)-10)+"px";
						if(parseInt(document.getElementById('emp-slider-count').style.top) < -(empMax)){
							document.getElementById('emp-slider-count').style.top = -(empMax)+"px";
						}
						document.getElementById('emp-slider-count').classList.remove('set-animate');
						window.clearTimeout( isScrolling );
						isScrolling = setTimeout(function() {
							document.getElementById('emp-slider-count').classList.add('set-animate');
							empCount++;
							
							if(empCount === 1){
								document.getElementById('emp-slider-count').style.top = -(empMax)+"px";
							}
							
						}, 500);
					}
				}
				
				
				
				
			}else {
				
				/*if(window.scrollY <= financialY){
					if(financialCount > 0){
					//if(parseInt(document.getElementById('financial-slider-count').style.top) < 0){
						window.scrollTo(0,financialY);
						if(financialCount === 2){
							if(parseInt(document.getElementById('financial-slider-count').style.top) <= -250){
								document.getElementById('financial-slider-count').style.top = (parseInt(document.getElementById('financial-slider-count').style.top)+10)+"px";
							}
							if(parseInt(document.getElementById('financial-slider-count').style.top) >= -250){
								document.getElementById('financial-slider-count').style.top = -250+"px";
							}
						}
						if(financialCount === 1){
							if(parseInt(document.getElementById('financial-slider-count').style.top) <= 0){
								document.getElementById('financial-slider-count').style.top = (parseInt(document.getElementById('financial-slider-count').style.top)+10)+"px";
							}
							if(parseInt(document.getElementById('financial-slider-count').style.top) >= 0){
								document.getElementById('financial-slider-count').style.top = 0+"px";
							}
						}
						
						if(parseInt(document.getElementById('financial-slider-count').style.top) > 0){
							document.getElementById('financial-slider-count').style.top = 0+"px";
						}
						document.getElementById('financial-slider-count').classList.remove('set-animate');
						window.clearTimeout( isScrolling );
						isScrolling = setTimeout(function() {
							document.getElementById('financial-slider-count').classList.add('set-animate');
							financialCount--;
							console.log(financialCount);
							if(financialCount === 1){
								document.getElementById('financial-slider-count').style.top = -250+"px";
							}
							if(financialCount === 0){
								document.getElementById('financial-slider-count').style.top = 0+"px";
							}
						}, 500);
					}
				}*/
				if(window.scrollY <= empY){
					if(parseInt(document.getElementById('emp-slider-count').style.top) < 0){
						window.scrollTo(0,empY);
						document.getElementById('emp-slider-count').style.top = (parseInt(document.getElementById('emp-slider-count').style.top)+10)+"px";
						if(parseInt(document.getElementById('emp-slider-count').style.top) > 0){
							document.getElementById('emp-slider-count').style.top = 0+"px";
						}
						document.getElementById('emp-slider-count').classList.remove('set-animate');
						window.clearTimeout( isScrolling );
						isScrolling = setTimeout(function() {
							document.getElementById('emp-slider-count').classList.add('set-animate');
							empCount--;
							console.log(financialCount+'      '+document.getElementById('financial-slider-count').style.top);
							if(empCount === 0){
								document.getElementById('emp-slider-count').style.top = 0+"px";
							}
							
						}, 500);
					}
				}
			}
		}
		
	});
	var windowScroll = false;
	var scrollableElement = window;
	
	scrollableElement.addEventListener('wheel', findScrollDirectionOtherBrowsers);
	
	function findScrollDirectionOtherBrowsers(event){
		var delta;
		
		if (event.wheelDelta){
			delta = event.wheelDelta;
		}else{
			delta = -1 * event.deltaY;
		}
		
		if (delta < 0){
			//console.log("DOWN");
			windowScroll = true;
			
		
		}else if (delta > 0){
			windowScroll = false;
			//console.log("UP");
			
		}
		
	}
	
	

  }
}


export default App;

