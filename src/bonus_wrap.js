import React, { Component } from 'react';
import desk_5_percentage from './images/desk-5-percentage.png';
import mob_5_percentage from './images/mob-5-percentage.png';

class BonusWrap extends Component {
  render() {
    return (
      <div className="bonus-wrap clearfix">
	  	<div className="inner-wrap">
        	<div className="bonus-heading">MoneyLion Bonus Features</div>
            <div className="w-25 bonus-desc">
            	<div className="bonus-sub-heading">Checking</div>
                <p>No fees</p>
                <p>55K+ free ATMs</p>
                <p>Full FDIC insurance</p>
                <p>MoneyLion Visa <br />Debit Card</p>
                <p>Bill pay</p>
                <p>Mobile deposit capture</p>
            </div>
            <div className="w-25 bonus-desc">
	            <div className="bonus-sub-heading">Credit</div>
                <p>$500 loans at 5.99% APR</p>
                <p>$500-$3,000+ loans <br />at belowmarket rates</p>
                <p>Funding in seconds</p>
                <p>No origination fees</p>
                <p>0% APR cash <br />advances up to 50% <br />of earned wages</p>
                <p>Credit building with <br />three-bureau reporting</p>
            </div>
            <div className="w-25 bonus-desc">
    			<div className="bonus-sub-heading">Investing</div>
                <p>Free managed <br />investment account</p>
                <p>No investment minimum</p>
                <p>Investment strategies <br />tailored to individual <br />members with <br />auto-rebalance</p>
                <p>An institutional approach <br />featuring customized <br />asset allocation models</p>
                <p>Ongoing monitoring <br />of portfolio and <br />underlying Investments</p>
                <p>MoneyLion Match<sup className="small">SM</sup></p>
            </div>
            <div className="w-25 bonus-desc">
            	<div className="bonus-sub-heading">Rewards</div>
                <p>Up to 15% cashback <br />at 20K+ retailers</p>
                <p>Premium access <br />to events</p>
                <p>Reward points <br />for financially <br />responsible decisions, <br />redeemable for gift <br />cards to major retailers</p>
            </div>
            <div className="match-wrap">
            	<div className="w-50">
                	<div className="match-count"><img alt="" src={desk_5_percentage} className="desktop" /><img alt="" src={mob_5_percentage} className="mobile" /></div>
                </div>
                <div className="w-50">
					<div className="match-outer-wrap">
						<div>
							<div className="match-heading">MoneyLion Match <br />gives investments <br />a boost</div>
							<div className="match-desc">While their money is earning its keep, we're <br />paying it overtime. MoneyLion Match<sup className="small">SM</sup> <br />gives members an extra 5% on investments-<br />up to $50 annually.</div>
						</div>
					</div>						
                </div>
            </div>
			</div>	
        </div>
    );
  }
}


export default BonusWrap;
