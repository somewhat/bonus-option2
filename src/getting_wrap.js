import React, { Component } from 'react';

class GettingWrap extends Component {
  render() {
    return (
      <div className="getting-started-wrap">
        	<div className="getting-started-heading">Getting Started</div>
            <p>Unlike other solutions, MoneyLion Bonus <br />offers a completely seamless deployment:</p>
            <div className="getting-started-heading-big">No integration<br />No implementation<br /><span>No cost to <br className="mobile" />the employer</span></div>
            <p>Your employees can be saving, investing, <br />borrowing and earning rewards in less than <br />24 hours.</p>
        </div>
    );
  }
}


export default GettingWrap;
