import React, { Component } from 'react';


class FormsWrap extends Component {
   handleClick = (e) => {
    if(e.target.attributes.getNamedItem('id').value === 'tab-1'){
		document.getElementById('tab-2').classList.remove('active');
		document.getElementById('tab-1').classList.add('active');
		document.getElementById('tab-highlight').classList.remove('right');
		document.getElementById('tab-2-wrap').classList.remove('active');
		setTimeout(() => {
  			document.getElementById('tab-1-wrap').classList.add('active');
		}, 500);
		
	}else {
		document.getElementById('tab-1').classList.remove('active');
		document.getElementById('tab-2').classList.add('active');
		document.getElementById('tab-highlight').classList.add('right');
		document.getElementById('tab-1-wrap').classList.remove('active');
		setTimeout(() => {
  			document.getElementById('tab-2-wrap').classList.add('active');
		}, 500);
	}
  }
  requestClick = (e) => {
	  var elements = document.getElementsByClassName('error-msg');
		while(elements.length > 0){
			elements[0].parentNode.removeChild(elements[0]);
		}
	  var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
	  if(document.getElementById('requestName').value === ''){
		  document.getElementById('requestName').classList.add('error');
		  const newElement = document.createElement('span');
		  newElement.className = 'error-msg';
		  newElement.innerText = 'Please enter the Name';
		  document.getElementById('requestName').parentNode.appendChild(newElement);
	  }
	  if(document.getElementById('requestEmail').value === ''){
		  document.getElementById('requestEmail').classList.add('error');
		  const newElement = document.createElement('span');
		  newElement.className = 'error-msg';
		  newElement.innerText = 'Please enter the Email address';
		  document.getElementById('requestEmail').parentNode.appendChild(newElement);
	  }else if(document.getElementById('requestEmail').value.match(mailformat)){
		  document.getElementById('requestEmail').classList.remove('error');
	  }else {
		 document.getElementById('requestEmail').classList.add('error');
		 const newElement = document.createElement('span');
		  newElement.className = 'error-msg';
		  newElement.innerText = 'Please enter valid Email address';
		  document.getElementById('requestEmail').parentNode.appendChild(newElement) 
	  }
  }
  shareClick = (e) => {
	  var elements = document.getElementsByClassName('error-msg');
		while(elements.length > 0){
			elements[0].parentNode.removeChild(elements[0]);
		}
	  var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
	  if(document.getElementById('shareName').value === ''){
		  document.getElementById('shareName').classList.add('error');
		  const newElement = document.createElement('span');
		  newElement.className = 'error-msg';
		  newElement.innerText = 'Please enter the Name';
		  document.getElementById('shareName').parentNode.appendChild(newElement);
	  }
	  if(document.getElementById('shareContact').value === ''){
		  document.getElementById('shareContact').classList.add('error');
		  const newElement = document.createElement('span');
		  newElement.className = 'error-msg';
		  newElement.innerText = 'Please enter the HR contact';
		  document.getElementById('shareContact').parentNode.appendChild(newElement);
	  }
	  if(document.getElementById('shareEmail').value === ''){
		  document.getElementById('shareEmail').classList.add('error');
		  const newElement = document.createElement('span');
		  newElement.className = 'error-msg';
		  newElement.innerText = 'Please enter the Email address';
		  document.getElementById('shareEmail').parentNode.appendChild(newElement);
	  }else if(document.getElementById('shareEmail').value.match(mailformat)){
		  document.getElementById('shareEmail').classList.remove('error');
	  }else {
		 document.getElementById('shareEmail').classList.add('error');
		 const newElement = document.createElement('span');
		  newElement.className = 'error-msg';
		  newElement.innerText = 'Please enter valid Email address';
		  document.getElementById('shareEmail').parentNode.appendChild(newElement) 
	  }
	  if(document.getElementById('shareContactEmail').value === ''){
		  document.getElementById('shareContactEmail').classList.add('error');
		  const newElement = document.createElement('span');
		  newElement.className = 'error-msg';
		  newElement.innerText = 'Please enter the Contact Email address';
		  document.getElementById('shareContactEmail').parentNode.appendChild(newElement);
	  }else if(document.getElementById('shareContactEmail').value.match(mailformat)){
		  document.getElementById('shareContactEmail').classList.remove('error');
	  }else {
		 document.getElementById('shareContactEmail').classList.add('error');
		 const newElement = document.createElement('span');
		  newElement.className = 'error-msg';
		  newElement.innerText = 'Please enter valid Contact Email address';
		  document.getElementById('shareContactEmail').parentNode.appendChild(newElement) 
	  }
  }
  keypressFunction = (e) => {
	  e.target.classList.remove('error');
	  var elements = e.target.parentNode.getElementsByClassName('error-msg');
	  while(elements.length > 0){
			elements[0].parentNode.removeChild(elements[0]);
		}
		
  }
  render() {
    return (
      <div className="form-wrap clearfix">
        	<div className="form-tab">
            	<div className="form-tab-highlight" id="tab-highlight"></div>
            	<div id="tab-1" className="form-tab-1 w-50 active" onClick={this.handleClick}>For Employers</div>
                <div id="tab-2" className="form-tab-2 w-50" onClick={this.handleClick}>For Employees</div>
            </div>
            <div className="form-1 active" id="tab-1-wrap">
            	<div className="w-47"><input type="text" className="text-box" placeholder="Name*" id="requestName" onKeyPress={this.keypressFunction} /></div>
                <div className="w-6">&nbsp;</div>
                <div className="w-47"><input type="text" className="text-box" placeholder="Company name" /></div>
                <div className="w-47"><label>&nbsp;</label><input type="text" className="text-box" placeholder="Email address*" id="requestEmail" onKeyPress={this.keypressFunction}/></div>
                <div className="w-6">&nbsp;</div>
                <div className="w-47"><label>How many employess are at your company?</label><select className="select-box"><option>Choose amount</option><option>0-500</option><option>500+</option></select></div>
                <div className="w-100"><input type="submit" value="Request a demo" className="submit-btn" onClick={this.requestClick} /></div>
            </div>
            <div className="form-2" id="tab-2-wrap">
            	<p>If you'd like your employer to offer MoneyLion Bonus, <br />share this page with someone in your company's <br />HR department.</p>
            	<div className="w-47"><input type="text" className="text-box" placeholder="Your name*" id="shareName" onKeyPress={this.keypressFunction}/></div>
                <div className="w-6">&nbsp;</div>
                <div className="w-47"><input type="text" className="text-box" placeholder="HR contact*" id="shareContact" onKeyPress={this.keypressFunction}/></div>
                <div className="w-47"><input type="text" className="text-box" placeholder="Your email address*" id="shareEmail" onKeyPress={this.keypressFunction}/></div>
                <div className="w-6">&nbsp;</div>
                <div className="w-47"><input type="text" className="text-box" placeholder="HR contact email address*" id="shareContactEmail" onKeyPress={this.keypressFunction}/></div>
                <div className="w-100"><input type="submit" value="Share this page" className="submit-btn" onClick={this.shareClick}/></div>
            </div>
        </div>
    );
  }
}


export default FormsWrap;
