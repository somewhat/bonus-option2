import React, { Component } from 'react';
import Slider from "react-slick";
import "slick-carousel/slick/slick.css"; 
import "slick-carousel/slick/slick-theme.css";

import slider_1 from './images/Bank-Slide-1.png';
import slider_2 from './images/Bank-Slide-2.png';
import slider_3 from './images/Bank-Slide-3.png';
import slider_4 from './images/Bank-Slide-4.png';

import slider_1_mob from './images/Bank-Slide-1-mob.png';
import slider_2_mob from './images/Bank-Slide-2-mob.png';
import slider_3_mob from './images/Bank-Slide-3-mob.png';
import slider_4_mob from './images/Bank-Slide-4-mob.png';

class BusinessSlider extends Component {
  render() {
	  const settings = {
      dots: false,
      infinite: true,
	  autoplay: true,
      speed: 1000,
	  autoplaySpeed:2000,
      slidesToShow: 1,
      slidesToScroll: 1,
	  pauseOnHover: true,
	  arrows:false,
	  fade: true,
    };
	
    return (
      <div className="business-wrap">
	  	<div className="inner-wrap">
        	<div className="w-45 business-text-wrap">
            	<div>
                	<div className="text-heading" >Free your business <br />to focus on what's <br />important</div>
                    <p>MoneyLion Bonus is a company-sponsored financial <br />wellness program designed to increase retention, reduce <br />recruiting costs, and bolster productivity-at no cost to <br />the employer or the employee.</p>
					<p>Stress is the #1 cause of health-related absences in the <br />workplace, and money is the #1 cause of stress. Free your <br />employees from the most common financial pitfalls and <br />empower them to focus on what's important-their <br />savings, stability, and peace of mind.</p>
				</div>                 
            </div>
            <div className="w-55 business-slider-wrap desktop">
            	<Slider className="business-slider" ref={slider => (this.slider = slider)} {...settings}>
                	<div>
                    	<div className="slider-img-wrap w-60">
                        	<img alt="" src={slider_1} className="slider-img" />
                        </div>
                        <div className="slider-desc w-40">
                            <div>
                                <p className="active">Sustainable path<br />to savings</p>
								<p >Managed investment<br />account</p>
                                <p>Access to premium<br />products and rewards</p>
                                <p>Financial literacy<br />program</p>
                            </div>
                        </div>
                    </div>
					<div>
                    	<div className="slider-img-wrap w-60">
                        	<img alt="" src={slider_2} className="slider-img" />
                        </div>
                        <div className="slider-desc w-40">
                            <div>
                                <p>Sustainable path<br />to savings</p>
                                <p className="active">Managed investment<br />account</p>
                                <p>Access to premium<br />products and rewards</p>
                                <p>Financial literacy<br />program</p>
                            </div>
                        </div>
                    </div>
                    <div>
                    	<div className="slider-img-wrap w-60">
                        	<img alt="" src={slider_3} className="slider-img" />
                        </div>
                        <div className="slider-desc w-40">
                            <div>
                                <p>Sustainable path<br />to savings</p>
                                <p>Managed investment<br />account</p>
                                <p className="active">Access to premium<br />products and rewards</p>
                                <p>Financial literacy<br />program</p>
                            </div>
                        </div>
                    </div>
					<div>
                    	<div className="slider-img-wrap w-60">
                        	<img alt="" src={slider_4} className="slider-img" />
                        </div>
                        <div className="slider-desc w-40">
                            <div>
                                <p>Sustainable path<br />to savings</p>
                                <p>Managed investment<br />account</p>
                                <p>Access to premium<br />products and rewards</p>
                                <p className="active">Financial literacy<br />program</p>
                            </div>
                        </div>
                    </div>
				</Slider>
				<div className="sliderLinks">
					<div>
						<p onClick={e => this.slider.slickGoTo('0')} value='2'>Sustainable path<br />to savings</p>
						<p onClick={e => this.slider.slickGoTo('1')} value='2'>Managed investment<br />account</p>
						<p onClick={e => this.slider.slickGoTo('2')} value='2'>Access to premium<br />products and rewards</p>
						<p onClick={e => this.slider.slickGoTo('3')} value='2'>Financial literacy<br />program</p>
					</div>
				</div>	                                                                       
            </div>
            <div className="mobile">
            	<div className="business-img"><img alt="" src={slider_1_mob} className="slider-img" /><p>Sustainable path to savings</p></div>
                <div className="business-img"><img alt="" src={slider_2_mob} className="slider-img" /><p>Managed investment account</p></div>
                <div className="business-img"><img alt="" src={slider_3_mob} className="slider-img" /><p>Access to premium products and rewards</p></div>
                <div className="business-img"><img alt="" src={slider_4_mob} className="slider-img" /><p>Financial literacy program</p></div>
            </div>
			</div>
        </div>
    );
  }
}


export default BusinessSlider;
